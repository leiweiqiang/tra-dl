var app = angular.module('app', []);
app.controller('controller', function($scope, $http) {
	$scope.loadsuccess = false;
	$http.get('/ai/api/getframe?minute=0').success(
			function(data, status, headers, config) {
				$scope.frames = data.frames;
				$scope.minute = data.minute;
				$scope.loadsuccess = true;
			}).error(function(data, status, headers, config) {
	});

	$scope.frame_check_click = function($frame) {
		$frame.gt = $frame.gt==1?0:1;
	};

	$scope.is_gt = function($frame) {
		return $frame.gt == 1;
	};

	$scope.status_change = function($frame, $status) {
		if ($status == 'In') {
			for (var i = $frame.id; i >= $scope.frames[0].id; i--) {
				if ($scope.frames[i] && $scope.frames[i].hasOwnProperty('status')) {
					j = i;
					break;
				}
			}
			$frame.status = $status;
			for (var i = j; i < $frame.id; i++) {
				$scope.frames[i].gt = 1;
			}
		}

		if ($status == 'Out') {
			$frame.status = $status;
			var j = $scope.frames.length - 1;
			for (var i = $frame.id; i < j; i++) {
				$scope.frames[i].gt = 0;
			}
		}

		if ($status == 'Clear') {
			$frame.status = '';

		}


	};

	$scope.next_btn_click = function($frames){
		$http.post('api/saveframe', $frames).
						 success(function(data) {
								 console.log(data);
						 }).
						 error(function(err) {
								 //错误代码
						 });
	};

	$('#frameModal').on('show.bs.modal', function (event) {
		var img = $(event.relatedTarget);
		var filename = img.data('filename');
		var modal = $(this);
		modal.find('img').attr('src', filename);
	})
});
