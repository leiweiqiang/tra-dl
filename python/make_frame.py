import sys, os, getopt
import mysql.connector
import json

def create_mysql_connector():
    return mysql.connector.connect(user='dl', password='123456',
                                  host='192.168.1.55',
                                  database='dl')

def make_movie_frame(movie_id, filename):
    cnx = create_mysql_connector()
    cursor_model = cnx.cursor()
    # cursor_model.execute("select imdb_id from movie where id=" + movie_id)
    # model = cursor_model.fetchone()
    # imdb_id = model[0]
    print (movie_id)
    medadata_results = os.popen(
        'ffprobe -v quiet -print_format json -show_format -show_streams ../movies/' + filename).read()
    print(medadata_results)
    metadata = json.loads(medadata_results)
    coded_width = metadata['streams'][0]['width']
    coded_height = metadata['streams'][0]['height']
    duration = metadata['format']['duration']
    w = "720"
    h = str(int(720 * int(coded_height) / int(coded_width)))
    w1 = "360"
    h1 = str(int(360 * int(coded_height) / int(coded_width)))
    cursor_model.execute("update movie set w=" + w + " , h=" + h + " , duration=" + str(duration) + " where id=" + movie_id)
    cnx.commit()

    #nb_frames = metadata['streams'][0]['nb_frames']

    #os.system('rm -rf ../' + movie_id)
    #os.system('mkdir ../' + movie_id)
    #os.system('mkdir ../' + movie_id + '/all_frame')
    #os.system('mkdir ../' + movie_id + '/thumb_frame')

    #os.system(
    #    'ffmpeg -i ../movies/' + filename + ' -s ' + w + '*' + h + ' -qscale:v 2 ../' +
    #    movie_id + '/all_frame/' + movie_id + '_%06d.JPEG')

    #os.system(
    #    'ffmpeg -i ../movies/' + filename + ' -s ' + w1 + '*' + h1 + ' -qscale:v 2 ../' +
    #    movie_id + '/thumb_frame/' + movie_id + '_%06d.JPEG')

    #os.system(
    #    'ffprobe -show_frames -select_streams v:0 -print_format csv ../movies/' +
    #    filename + ' > ../movie_info/' + movie_id + '.txt')

    cnt = 0
    with open('../movie_info/' + movie_id + '.txt', 'r') as fh:
        for line in fh:
            cnt += 1
            data = line.split(',')
            sql = 'insert into frame (movie_id, frame_index, keyframe) VALUES (' + movie_id + ',' + str(cnt) + ',' + data[3] + ')'
            print(sql)
            cursor_model.execute(sql)
    cursor_model.execute("update movie set flag=1 where id=" + movie_id)
    cursor_model.close()
    cnx.commit()
    cnx.close()


def main():
    cnx = create_mysql_connector()
    cursor = cnx.cursor()
    cursor.execute("select * from movie where flag=0")

    for row in cursor:
        print(row)
        make_movie_frame(str(row[0]), row[6])

if __name__ == '__main__':
    main()
