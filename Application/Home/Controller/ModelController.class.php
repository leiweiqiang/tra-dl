<?php

namespace Home\Controller;

use Think\Controller;

class ModelController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_models()
    {
        $models = M("model")->order('classes,subtotal')->select();
        for ($i = 0; $i < count($models); $i++) {
            $models[$i]['class_cnt'] = M("model_classes")->where('model_id='.$models[$i]['id'])->count();
        }
        $ret['models'] = $models;
        echo json_encode($ret);
    }

    private function get_training_data_cnt($model_id, $class_id){
        $Model = new \Think\Model();
        $sql = 'SELECT count(*) FROM training_annotation JOIN annotation ON annotation.id = training_annotation.annotation_id WHERE training_annotation.model_id = $model_id and annotation.class_id = $class_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    private function get_testing_data_cnt($model_id, $class_id){
        $Model = new \Think\Model();
        $sql = 'SELECT count(*) FROM testing_annotation JOIN annotation ON annotation.id = testing_annotation.annotation_id WHERE testing_annotation.model_id = $model_id and annotation.class_id = $class_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_model_classes($model_id)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT class.*, movie.id as movie_id FROM model_classes JOIN class ON class.id = model_classes.class_id JOIN movie ON movie.id = class.movie_id WHERE model_classes.model_id = $model_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $classes = $Model->query($sql);

        for ($i = 0; $i < count($classes); $i++) {
            $classes[$i]['training_data_cnt'] = $this->get_training_data_cnt($model_id, $classes[$i]['id']);
            $classes[$i]['testing_data_cnt'] = $this->get_testing_data_cnt($model_id, $classes[$i]['id']);
        }

        $ret['classes'] = $classes;
        echo json_encode($ret);
    }

    public function add_one_training_data_save($model_id = 0, $annotation_id = 0)
    {
        $data['annotation_id'] = intval($annotation_id);
        $data['model_id'] = intval($model_id);
        $training_annotation = M("training_annotation");
        $train = $training_annotation->add($data);
        echo json_encode($train);
    }

    public function add_one_training_data($model_id = 0, $movie_id = 0, $class_id = 0, $total = 1)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT annotation.*, movie.id as movie_id FROM annotation JOIN movie ON movie.id = annotation.movie_id 
WHERE annotation.id NOT IN (SELECT annotation_id AS id FROM training_annotation) 
AND annotation.frame_index NOT IN (SELECT frame_index FROM annotation_temp) 
AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id ORDER BY rand() LIMIT $total';
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$total', $total, $sql);
        $ret = $Model->query($sql);
        $new['data'] = $ret[0];

        $sql = 'SELECT count(*) FROM annotation JOIN movie ON movie.id = annotation.movie_id 
WHERE annotation.id NOT IN (SELECT annotation_id AS id FROM training_annotation) 
AND annotation.frame_index NOT IN (SELECT frame_index FROM annotation_temp) 
AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id ORDER BY rand() LIMIT $total';
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$total', $total, $sql);
        $ret = $Model->query($sql);
        $new['remain'] = $ret[0]['count(*)'];

        $sql = "select annotation.*, movie.id as movie_id from annotation join training_annotation on annotation.id = training_annotation.annotation_id join movie on movie.id = annotation.movie_id where training_annotation.model_id = $model_id and annotation.frame_id < " . $new['data']['frame_id'] . " order by annotation.frame_id desc limit 1";
        $sql = str_replace('$model_id', $model_id, $sql);
        $ret = $Model->query($sql);
        $new['prev'] = $ret[0];

        $sql = "select annotation.*, movie.id as movie_id from annotation join training_annotation on annotation.id = training_annotation.annotation_id join movie on movie.id = annotation.movie_id where training_annotation.model_id = $model_id and annotation.frame_id > " . $new['data']['frame_id'] . " order by annotation.frame_id limit 1";
        $sql = str_replace('$model_id', $model_id, $sql);
        $ret = $Model->query($sql);
        $new['next'] = $ret[0];

        echo json_encode($new);
    }

    private function get_training_cnt($model_id = 0, $class_id = 0){
        $sql = 'SELECT count(*) FROM training_annotation
JOIN annotation ON annotation.id = training_annotation.annotation_id
JOIN class ON annotation.movie_id = class.movie_id
WHERE training_annotation.model_id = $model_id
AND class.id = $class_id
ORDER BY annotation.frame_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        $count = $ret[0]['count(*)'];
        return $count;
    }

    public function get_training_data($model_id = 0, $class_id = 0, $page = 0)
    {
        $page_cnt = 40;
        $sql = 'SELECT annotation.*, class.movie_id as movie_id FROM training_annotation
JOIN annotation ON annotation.id = training_annotation.annotation_id
JOIN class ON annotation.movie_id = class.movie_id
WHERE training_annotation.model_id = $model_id
AND class.id = $class_id
ORDER BY annotation.frame_id
LIMIT $page_cnt
OFFSET $offset
';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$page_cnt', $page_cnt, $sql);
        $sql = str_replace('$offset', $page_cnt * $page, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        $count = $this->get_training_cnt($model_id, $class_id);

        $ret['pages'] = $this->get_pages($page_cnt, $page, $count);
        $ret['training_data_total'] = $count;

        echo json_encode($ret);

    }

    public function get_testing_data($model_id = 0, $class_id = 0)
    {
        $sql = 'SELECT annotation.*, class.movie_id as movie_id FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
JOIN class ON annotation.movie_id = class.movie_id
WHERE testing_annotation.model_id = $model_id
AND class.id = $class_id
ORDER BY annotation.frame_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        echo json_encode($ret);

    }

    private function _save($input)
    {
        if ($input['id'] == -1) {
            unset($input['id']);
            if ($input['x1'] == 0 && $input['y1'] == 0 && $input['x2'] == 0 && $input['y2'] == 0) {
                return;
            } else {
                $annotation = M("annotation");
                $frame =  M("frame")->where('frame_index=' . $input['frame_index'] . ' and movie_id=' . $input['movie_id'])->find();
                $input['frame_id'] = $frame['id'];
                $annotation->field('class_id,frame_id,frame_index,movie_id,w,h,x1,y1,x2,y2')->add($input);
            }
        } else {
            $annotation = M("annotation");
            $annotation->where('id=%d', $input['id'])->field('id,class_id,frame_id,frame_index,movie_id,w,h,x1,y1,x2,y2')->save($input);
        }
        echo $annotation->getLastSql();
    }

    public function save()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $this->_save($input);
        }
        return true;
    }

    public function delete()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $annotation = M("annotation");
            $annotation->where('id=' . $input['id'])->delete();
            echo $annotation->getLastSql();
        }
    }


    private function get_insert_report_dataset_sql($model_id, $classes){
        $test_sql = '';

        $sql = 'insert into report_dataset (annotation_id, report_id, model_id)
select id as annotation_id, 2, $model_id from `annotation` where class_id =$class_id
ORDER BY rand()
limit 200';

        foreach ($classes as $class){
            $temp = str_replace('$model_id', $model_id, $sql);
            $temp = str_replace('$class_id', $class['id'], $temp);
            $test_sql = $test_sql . $temp . ';';
        }
        return $test_sql;
    }

    public function get_action($model_id = 0)
    {
//        $sql = 'DELETE * FROM testing_annotation
//JOIN annotation on annotation.id = testing_annotation.annotation_id
//WHERE testing_annotation.model_id = $model_id
//AND annotation.class_id = $class_id';
//        $sql = 'DELETE FROM testing_annotation
//WHERE testing_annotation.model_id = $model_id';
//        $sql = str_replace('$model_id', $model_id, $sql);
//        $sql = str_replace('$class_id', $class_id, $sql);

//        $Model = new \Think\Model();
//        $ret = $Model->query($sql);

        $model = M('model')->where('id=' . $model_id)->find();


        $sql1 = 'insert into testing_annotation (annotation_id, model_id)
select id as annotation_id,$model_id from `annotation` where class_id =$class_id
and id not in (select annotation_id from training_annotation where model_id = $model_id)
ORDER BY rand()
limit ' . $model['subtotal'];


//        $sql1 = str_replace('$model_id', $model_id, $sql1);
//        $sql1 = str_replace('$class_id', $class_id, $sql1);
//
//        $ret = $Model->query($sql);
//
//        echo json_encode($ret);

        $training_sql = '';

        $Model = new \Think\Model();
        $sql = 'SELECT class.*, movie.id as movie_id FROM model_classes JOIN class ON class.id = model_classes.class_id JOIN movie ON movie.id = class.movie_id WHERE model_classes.model_id = $model_id';
        $sql = str_replace('$model_id', $model_id, $sql);


        $classes = $Model->query($sql);

        foreach ($classes as $class){
            $temp = str_replace('$model_id', $model_id, $sql1);
            $temp = str_replace('$class_id', $class['id'], $temp);
            $training_sql = $training_sql . $temp . ';';
        }

        $ret['training_data'] = $training_sql;
        $ret['testing_data'] = str_replace('testing_annotation', 'training_annotation', $training_sql);
        $ret['report_dataset'] = $this->get_insert_report_dataset_sql($model_id, $classes);

        $class_base = 1;
        if ($model['classes'] == 9){
            $class_base = 3;
        }else if($model['classes'] == 30){
            $class_base = 19;
        }
        $ret['insert_model_classes'] = "insert into model_classes (model_id, class_id) SELECT $model_id, class_id FROM model_classes WHERE model_id=" . $class_base;
        echo json_encode($ret);

    }

    public function get_pages($page_cnt, $page, $count)
    {
        $pages = array();
        for ($i = 0; $i < ($count / $page_cnt); $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i < (($count / $page_cnt)-1)){
                $data['title'] = $i*$page_cnt . '~' . ($i+1)*$page_cnt ;
            }else{
                $data['title'] = $i*$page_cnt . '~' . $count ;
            }
            $data['cnt'] = $page_cnt;
            if ($i == $page) {
                $data['class'] = 'btn btn-default btn-xs active';
            } else {
                $data['class'] = 'btn btn-default btn-xs ';
            }
            array_push($pages, $data);
        }
        return $pages;
    }

}
