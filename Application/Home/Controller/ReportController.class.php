<?php

namespace Home\Controller;

use Think\Controller;

class ReportController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_reports()
    {
        $reports = M("report")->select();
        for ($i = 0; $i < count($reports); $i++) {
            $reports[$i]['report_data_cnt'] = $this->get_report_data_cnt($reports[$i]['id']);
        }
        $ret['reports'] = $reports;
        echo json_encode($ret);
    }

    private function get_report_data_cnt($report_id)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT count(*) FROM report_dataset WHERE report_dataset.report_id = $report_id';
        $sql = str_replace('$report_id', $report_id, $sql);
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    private function get_report_class_data_cnt($report_id, $class_id)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT count(*) FROM report_dataset 
JOIN annotation ON annotation.id = report_dataset.annotation_id
JOIN class ON class.id = annotation.class_id
WHERE report_dataset.report_id = $report_id AND class.id = $class_id';
        $sql = str_replace('$report_id', $report_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_report_classes($report_id)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT DISTINCT class.*
FROM report_dataset 
JOIN annotation ON annotation.id = report_dataset.annotation_id
JOIN class ON class.id = annotation.class_id
WHERE report_dataset.report_id = $report_id';
        $sql = str_replace('$report_id', $report_id, $sql);
        $classes = $Model->query($sql);

        for ($i = 0; $i < count($classes); $i++) {
            $classes[$i]['class_data_cnt'] = $this->get_report_class_data_cnt($report_id, $classes[$i]['id']);
        }

        $ret['classes'] = $classes;
        echo json_encode($ret);
    }

    public function get_report_iou($weights_id, $movie_id, $frame_index)
    {
        $where['weights_id'] = $weights_id;
        $where['frame_id'] = $frame_index;
        $where['movie_id'] = $movie_id;
        $report_iou = M("report_iou")->where($where)->order('confidence desc')->limit(1)->select();
        return $report_iou[0];
    }

    public function cacl_iou($annotation)
    {

        $x1 = $annotation['x1'];
        $y1 = $annotation['y1'];
        $x2 = $annotation['x2'];
        $y2 = $annotation['y2'];

        $_x1 = $annotation['result']['x1'];
        $_y1 = $annotation['result']['y1'];
        $_x2 = $annotation['result']['x2'];
        $_y2 = $annotation['result']['y2'];

        $areai = ($x2 - $x1 + 1) * ($y2 - $y1 + 1);
        $areaj = ($_x2 - $_x1 + 1) * ($_y2 - $_y1 + 1);

        $xx1 = max($x1, $_x1);
        $yy1 = max($y1, $_y1);
        $xx2 = min($x2, $_x2);
        $yy2 = min($y2, $_y2);

        $h = max(0, $yy2 - $yy1 + 1);
        $w = max(0, $xx2 - $xx1 + 1);

        $intersection = $w * $h;

        $iou = $intersection / ($areai + $areaj - $intersection);

        return $iou;
    }


    public function get_report_data($report_id = 0, $class_id = 0, $weights_id = 0, $page = 0)
    {
        $page_cnt = 40;
        $sql = 'SELECT annotation.*, class.movie_id AS movie_id 
FROM report_dataset
JOIN annotation ON annotation.id = report_dataset.annotation_id
JOIN class ON class.id = annotation.class_id
WHERE report_dataset.report_id = $report_id
AND class.id = $class_id
ORDER BY annotation.frame_id
LIMIT $page_cnt
OFFSET $offset';
        $sql = str_replace('$report_id', $report_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$page_cnt', $page_cnt, $sql);
        $sql = str_replace('$offset', $page_cnt * $page, $sql);

        $Model = new \Think\Model();
        $annotations = $Model->query($sql);

        $count = $this->get_training_cnt($report_id, $class_id);

        for ($i = 0; $i < count($annotations); $i++) {
            $annotations[$i]['result'] = $this->get_report_iou($weights_id, $annotations[$i]['movie_id'], $annotations[$i]['frame_index']);
            if ($annotations[$i]['result'] != null) {
                $annotations[$i]['iou'] = $this->cacl_iou($annotations[$i]);
            } else {
                $annotations[$i]['iou'] = 0;
            }
        }
        $ret['pages'] = $this->get_pages($page_cnt, $page, $count);
        $ret['training_data_total'] = $count;
        $ret['annotations'] = $annotations;
        echo json_encode($ret);

    }

    private function get_training_cnt($report_id = 0, $class_id = 0)
    {
        $sql = 'SELECT count(*)
FROM report_dataset
JOIN annotation ON annotation.id = report_dataset.annotation_id
JOIN class ON class.id = annotation.class_id
WHERE report_dataset.report_id = $report_id
AND class.id = $class_id';

        $sql = str_replace('$report_id', $report_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        $count = $ret[0]['count(*)'];
        return $count;
    }

    public function get_pages($page_cnt, $page, $count)
    {
        $pages = array();
        for ($i = 0; $i < ($count / $page_cnt); $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i < (($count / $page_cnt) - 1)) {
                $data['title'] = $i * $page_cnt . '~' . ($i + 1) * $page_cnt;
            } else {
                $data['title'] = $i * $page_cnt . '~' . $count;
            }
            $data['cnt'] = $page_cnt;
            if ($i == $page) {
                $data['class'] = 'btn btn-default btn-xs active';
            } else {
                $data['class'] = 'btn btn-default btn-xs ';
            }
            array_push($pages, $data);
        }
        return $pages;
    }

    public function get_model_weights($model_id)
    {
        $weights = M("weights")->where('model_id=' . $model_id)->select();
        for ($i = 0; $i < count($weights); $i++) {
            $weights[$i]['verify_cnt'] = M("verify_list")->where('weights_id=' . $weights[$i]['id'])->count();
            $weights[$i]['results_cnt'] = M("report_iou")->where('weights_id=' . $weights[$i]['id'])->count();
        }
        $ret['weights'] = $weights;
        echo json_encode($ret);
    }

    private function save_report_result($report, $weights, $annotations)
    {
        $result['training'] = $weights['training'];
        $result['report_id'] = $report['id'];
        $result['weights_id'] = $weights['id'];
        $result['tp'] = 0;
        $result['tn'] = 0;
        $result['fp'] = 0;
        $result['fn'] = 0;

        $model_classes = M('model_classes')->where("model_id = " . $weights['model_id'])->select();
        $classes_list = array();
        for ($i = 0; $i < count($model_classes); $i++) {
            array_push($classes_list, $model_classes[$i]['class_id']);
        }

        $subtotal = $report['subtotal'];
        $class_cnt = count($model_classes);

//        $Model = new \Think\Model();
//        $sql = 'SELECT model_classes.*
//FROM weights
//JOIN model_classes ON model_classes.model_id = weights.model_id
//WHERE weights.id = $weights_id';
//        $sql = str_replace('$weights_id', $weights['id'], $sql);
//        $classes = $Model->query($sql);

        foreach ($annotations as $annotation) {

//            if ($annotation['result'] != null){
//                //found
//                if ($annotation['result']['class_id'] == $annotation['class_id'] && $annotation['iou'] > 0.5) {
//                    $result['tp'] = $result['tp'] + 1;
//                } else if($annotation['result']['class_id'] == $annotation['class_id']){
//                    $result['fp'] = $result['fp'] + 1;
//                }
//                else {
//                    $result['tp'] = $result['tp'] + 1;
//                }
//            }else{
//                //not found
//                if (in_array($annotation['class_id'], $classes_list) ){
//                    $result['fn'] = $result['fn'] + 1;
//                }else{
//                    $result['tn'] = $result['tn'] + 1;
//                }
//            }

            if ($annotation['result'] != null){
                //found
                if ($annotation['result']['class_id'] == $annotation['class_id'] && $annotation['iou'] > 0.5) {
                    $result['tp'] = $result['tp'] + 1;
                } else {
                    $result['fp'] = $result['fp'] + 1;
                }
            }else{
                //not found
                $result['fp'] = $result['fp'] + 1;
            }
        }

        if($result['tp'] == 0 && $result['tn'] == 0 && $result['fp'] == 0){
            $result['percision'] = 0;
            $result['recall'] = 0;
            $result['f1'] = 0;
        }else{
            $result['percision'] = $result['tp'] / ($result['tp'] + $result['fp'] - $subtotal*($report['classes'] - $class_cnt));
            $result['recall'] = $result['tp'] / ($result['tp'] + $result['fn'] - $subtotal*($report['classes'] - $class_cnt));
            $result['f1'] = 2 * ($result['percision'] * $result['recall']) / ($result['percision'] + $result['recall']);
        }

        $result['weights_name'] = $weights['name'];
//        echo 'tp:' . $tp . ",";
//        echo '$tn:' . $tn . ",";
//        echo '$fp:' . $fp . ",";
//        echo '$fn:' . $fn . ",";
//        echo '$percision:' . $percision . ",";
//        echo '$recall:' . $recall . ",";
//        echo '$f1:' . $f1 . ",";

        $report_results = M("report_results");
        $ret = $report_results->field('report_id,weights_id,weights_name,training,tp,tn,fp,fn,percision,recall,f1,map')->add($result);
//        echo json_encode($ret);
    }


    private function load_report_weights($report, $weights)
    {
        $report_id = $report['id'];
        $weights_id = $weights['id'];
//
        $sql = 'SELECT annotation.*
FROM report_dataset
JOIN annotation ON annotation.id = report_dataset.annotation_id
WHERE report_dataset.report_id = $report_id';
        $sql = str_replace('$report_id', $report_id, $sql);

        $Model = new \Think\Model();
        $annotations = $Model->query($sql);
        for ($i = 0; $i < count($annotations); $i++) {
            $annotations[$i]['result'] = $this->get_report_iou($weights_id, $annotations[$i]['movie_id'], $annotations[$i]['frame_index']);
            if ($annotations[$i]['result'] != null) {
                $annotations[$i]['iou'] = $this->cacl_iou($annotations[$i]);
            } else {
                $annotations[$i]['iou'] = 0;
            }
        }

        $this->save_report_result($report, $weights, $annotations);

//        echo json_encode($annotations);
//        echo $sql . ', ';
    }

    private function load_report($report)
    {
        foreach (M("weights")->select() as $weights) {
            $where['weights_name'] = $weights['name'];
            $where['report_id'] = $report['id'];
            $where['weights_id'] = $weights['id'];
            if (M("report_results")->where($where)->count() == 0)
                $this->load_report_weights($report, $weights);
        }
    }

    public function reload_report()
    {
//        M("report_results")->where('1')->delete();

        foreach (M("report")->select() as $report) {
            $this->load_report($report);
        }
//        $reports = M("report")->select();
//        $this->load_report($reports[1]);
        echo 'success';
    }

    public function get_report_results($report_id, $weights_id){

        $sql = 'SELECT report_results.*, weights.name, model.subtotal, model.classes
FROM report_results
JOIN weights ON weights.id = report_results.weights_id
JOIN model ON  model.id = weights.model_id
WHERE report_results.report_id = $report_id 
ORDER BY model.subtotal';
        $sql = str_replace('$weights_id', $weights_id, $sql);
        $sql = str_replace('$report_id', $report_id, $sql);

        $Model = new \Think\Model();
        $results = $Model->query($sql);
        echo json_encode($results);
    }


    public function test()
    {
        $report_id = 1;
        $weights_id = 1;
//
        $sql = 'SELECT annotation.*
FROM report_dataset
JOIN annotation ON annotation.id = report_dataset.annotation_id
WHERE report_dataset.report_id = $report_id
ORDER BY frame_id';
        $sql = str_replace('$report_id', $report_id, $sql);

        $Model = new \Think\Model();
        $annotations = $Model->query($sql);
        for ($i = 0; $i < count($annotations); $i++) {
            $annotations[$i]['result'] = $this->get_report_iou($weights_id, $annotations[$i]['movie_id'], $annotations[$i]['frame_index']);
            if ($annotations[$i]['result'] != null) {
                $annotations[$i]['iou'] = $this->cacl_iou($annotations[$i]);
            } else {
                $annotations[$i]['iou'] = 0;
            }
        }

//        $this->save_report_result($report, $weights, $annotations);

        echo json_encode($annotations);
//        echo $sql . ', ';
    }


    public function get_models_class_cnt_list()
    {
        $Model = new \Think\Model();
        $sql = 'SELECT distinct classes from model';
        $ret = $Model->query($sql);
        return $ret;
    }

    public function get_report_results_by_class($report, $classes, $training)
    {
        $sql = 'SELECT report_results.*, weights.name, model.subtotal, model.classes
FROM report_results
JOIN weights ON weights.id = report_results.weights_id
JOIN model ON  model.id = weights.model_id
WHERE model.classes = $classes
AND weights.training = $training
AND report_results.report_id = $report_id
ORDER BY model.subtotal';
        $sql = str_replace('$classes', $classes, $sql);
        $sql = str_replace('$training', $training, $sql);
        $sql = str_replace('$report_id', $report['id'], $sql);

        $Model = new \Think\Model();
        $results = $Model->query($sql);
        return $results;
    }

    public function get_chart_data($training){
        $reports = M("report")->select();
        $class_cnt_list = $this->get_models_class_cnt_list();
        for ($i = 0; $i < count($reports); $i++) {
            for ($j = 0; $j < count($class_cnt_list); $j++) {
                    $class_cnt_list[$j]['results'] = $this->get_report_results_by_class($reports[$i], $class_cnt_list[$j]['classes'], $training);
            }
            $reports[$i]['class_cnt_list'] = $class_cnt_list;
        }
        echo json_encode($reports);
    }

}
