<?php

namespace Home\Controller;

use Think\Controller;

class ClassController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_classes()
    {
        $classes = M("class")->select();
        for ($i = 0; $i < count($classes); $i++) {
            $classes[$i]['annotation_cnt'] = M("annotation")->where('class_id='.$classes[$i]['id'])->count();
        }
        $ret['classes'] = $classes;
        $ret['total_annotation'] = M("annotation")->count();

        echo json_encode($ret);
    }

    public function get_annotations_count($movie_id, $class_id)
    {
        $where['movie_id'] = $movie_id;
        $where['class_id'] = $class_id;
        $count = M("annotation")->where($where)->count();
        return $count;
    }

    public function get_annotations($movie_id, $class_id, $page)
    {
        $page_cnt = 200;
        $where['movie_id'] = $movie_id;
        $where['class_id'] = $class_id;
        $annotations = M("annotation")->where($where)->order('frame_index')->limit($page*$page_cnt, $page_cnt)->select();
        $count = $this->get_annotations_count($movie_id, $class_id);
        $ret['annotations'] = $annotations;
        $ret['total'] = $count;
        $ret['pages'] = $this->get_pages($page_cnt, $page, $count);
        echo json_encode($ret);
    }

    public function get_weights_results($movie_id, $class_id)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT DISTINCT weights.* FROM results JOIN weights ON weights.id = results.weights_id WHERE results.movie_id = $movie_id AND results.class_id = $class_id';
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $ret = $Model->query($sql);
        echo json_encode($ret);
    }

    public function delete_annotation_temp(){
        $annotation = M("annotation_temp");
        $ret = $annotation->where('1')->delete();
        echo json_encode($ret);
    }

    public function add_annotation_temp($weights_id, $movie_id, $frame_id)
    {
        $annotation = M("annotation_temp");
        $input['weights_id'] = $weights_id;
        $input['movie_id'] = $movie_id;
        $input['frame_index'] = $frame_id;
        $annotation->field('weights_id,movie_id,frame_index')->add($input);
    }

    public function add_from_weights($weights_id = 0, $movie_id = 0, $class_id = 0, $confidence = 0.5, $total = 1)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT results.weights_id, results.weights_id, results.model_id, results.movie_id, results.class_id, results.confidence, results.x1, results.x2, results.y1, results.y2, results.frame_id as frame_index, movie.w, movie.h FROM results 
JOIN movie ON movie.id = results.movie_id 
WHERE results.frame_id NOT IN (SELECT frame_id FROM annotation WHERE annotation.movie_id = $movie_id) 
AND results.frame_id NOT IN (SELECT frame_index AS frame_id FROM annotation_temp) 
AND results.movie_id = $movie_id 
AND results.class_id = $class_id 
AND results.weights_id = $weights_id 
AND results.confidence > $confidence
ORDER BY rand() LIMIT $total';
        $sql = str_replace('$weights_id', $weights_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$confidence', $confidence, $sql);
        $sql = str_replace('$total', $total, $sql);
        $ret = $Model->query($sql);
        $new['data'] = $ret[0];

        $sql = "select annotation.* from annotation where annotation.movie_id = $movie_id and annotation.frame_index < " . $new['data']['frame_index'] . " order by annotation.frame_index desc limit 1";
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $ret = $Model->query($sql);
        $new['prev'] = $ret[0];

        $sql = "select annotation.* from annotation where annotation.movie_id = $movie_id and annotation.frame_index > " . $new['data']['frame_index'] . " order by annotation.frame_index limit 1";
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $ret = $Model->query($sql);
        $new['next'] = $ret[0];

        $this->add_annotation_temp($new['data']['weights_id'], $new['data']['movie_id'], $new['data']['frame_index']);

        echo json_encode($new);
//        echo $sql;
    }

    public function get_pages($page_cnt, $page, $count)
    {
        $pages = array();
        for ($i = 0; $i < ($count / $page_cnt); $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i < (($count / $page_cnt)-1)){
                $data['title'] = $i*$page_cnt . '~' . ($i+1)*$page_cnt ;
            }else{
                $data['title'] = $i*$page_cnt . '~' . $count ;
            }
            $data['cnt'] = $page_cnt;
            if ($i == $page) {
                $data['class'] = 'btn btn-default btn-xs active';
            } else {
                $data['class'] = 'btn btn-default btn-xs ';
            }
            array_push($pages, $data);
        }
        return $pages;
    }

    public function delete_annotation()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            M('annotation')->delete($input['id']);
        }
        return true;
    }
}
