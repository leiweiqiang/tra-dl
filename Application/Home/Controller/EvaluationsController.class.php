<?php

namespace Home\Controller;

use Think\Controller;

class EvaluationsController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_data()
    {
        $weights = M("weights")->select();
        $movies = M("movie")->select();
        $ret['weights'] = $weights;
        $ret['movies'] = $movies;
        echo json_encode($ret);
    }

    public function get_testing_data($weights = 0, $positives = 0, $negatives = 0)
    {
        $Model = new \Think\Model();
//        $sql = 'SELECT * FROM iou WHERE $weights = $weights and (movie_id = $positives or movie_id = $negatives) and confidence > 0.2';
        $sql = 'SELECT annotation.*, movie.id as movie_id FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
JOIN movie ON annotation.movie_id = movie.id
WHERE 
(annotation.movie_id = $positives OR annotation.movie_id = $negatives)
ORDER BY annotation.frame_index';

//        $sql = str_replace('$weights', $weights, $sql);
        $sql = str_replace('$positives', $positives, $sql);
        $sql = str_replace('$negatives', $negatives, $sql);
        $ret = $Model->query($sql);

//        $data['results'] = $this->load_results_data($weights, $positives, $negatives);
        $data['results'] = $this->load_results($ret, $weights, $positives);
        $data['testing_data'] = $ret;

        $positives_class = M("class")->where('movie_id = ' . $positives)->find();
        $data['positive_class'] = $positives_class['id'];
        echo json_encode($data);
    }

    public function get_iou($result)
    {

        $x1 = $result['x1'];
        $y1 = $result['y1'];
        $x2 = $result['x2'];
        $y2 = $result['y2'];

        $sql = 'SELECT annotation.* FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
WHERE annotation.frame_index =' . $result['frame_id'];

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        $_x1 = $ret[0]['x1'];
        $_y1 = $ret[0]['y1'];
        $_x2 = $ret[0]['x2'];
        $_y2 = $ret[0]['y2'];

        $areai = ($x2 - $x1 + 1) * ($y2 - $y1 + 1);
        $areaj = ($_x2 - $_x1 + 1) * ($_y2 - $_y1 + 1);

        $xx1 = max($x1, $_x1);
        $yy1 = max($y1, $_y1);
        $xx2 = min($x2, $_x2);
        $yy2 = min($y2, $_y2);

        $h = max(0, $yy2 - $yy1 + 1);
        $w = max(0, $xx2 - $xx1 + 1);

        $intersection = $w * $h;

        $iou = $intersection / ($areai + $areaj - $intersection);

        return $iou;
    }

    public function load_results($ret, $weights_id, $positives){
        $array = [];
        for ($i = 0; $i < count($ret); $i++) {
            $where['weights_id'] = $weights_id;
            $where['movie_id'] = $positives;
            $where['frame_id'] = $ret[$i]['frame_index'];
            $model = M("iou");
            $iou = $model->where($where)->find();
//            array_push($array, $model->getLastSql());
            if ($iou != null){
                $iou['iou'] = $this->get_iou($iou);
                array_push($array, $iou);
            }
        }
        return $array;
    }


    public function load_results_data($weights_id = 0, $positives = 0, $negatives = 0)
    {
        $positives_class = M("class")->where('movie_id = ' . $positives)->find();
        $negatives_class = M("class")->where('movie_id = ' . $negatives)->find();

        $where['weights_id'] = $weights_id;

        $where['_string'] = '(movie_id = $positives OR movie_id = $negatives) AND (class_id = $id_positives_class OR class_id = $id_negatives_class)';
        $where['_string']  = str_replace('$positives', $positives, $where['_string']);
        $where['_string']  = str_replace('$negatives', $negatives, $where['_string']);
        $where['_string']  = str_replace('$id_positives_class', $positives_class['id'], $where['_string']);
        $where['_string']  = str_replace('$id_negatives_class', $negatives_class['id'], $where['_string']);

//        $where['_string'] = '(movie_id = ' . $positives . ' or movie_id = ' . $negatives . ')' . ' and (class_id = ' . $positives_class['id'] . ' or class_id = ' . $negatives_class['id'] . ')';
        $where['confidence'] = array('gt', 0.2);
        $ret = M("iou")->where($where)->select();
        for ($i = 0; $i < count($ret); $i++) {
            $ret[$i]['iou'] = $this->get_iou($ret[$i]);
        }
        return $ret;
    }

    public function save_map($weights_id = 0, $movie_id = 0, $f1 = 0, $map = 0){
        $input['weights_id'] = $weights_id;
        $input['movie_id'] = $movie_id;
        $input['f1'] = $f1;
        $input['map'] = $map;

        $where['weights_id'] = $weights_id;
        $where['movie_id'] = $movie_id;
        $movie = M("evaluation")->where($where)->find();
        if (count($movie) == 0) {
            M("evaluation")->field('weights_id,movie_id,f1,map')->add($input);
        } else {
            M("evaluation")->where($where)->field('movie_id,f1,map')->save($input);
        }
    }

}
