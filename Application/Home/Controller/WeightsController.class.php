<?php

namespace Home\Controller;

use Think\Controller;

class WeightsController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_models()
    {
        $models = M("model")->order('classes, subtotal')->select();
        for ($i = 0; $i < count($models); $i++) {
            $models[$i]['weights_cnt'] = M("weights")->where('model_id=' . $models[$i]['id'])->count();
            $models[$i]['class_cnt'] = M("model_classes")->where('model_id=' . $models[$i]['id'])->count();
        }
        $ret['models'] = $models;
        echo json_encode($ret);
    }

    public function get_models_class_cnt_list()
    {
        $Model = new \Think\Model();
        $sql = 'SELECT distinct classes from model';
        $ret = $Model->query($sql);
        echo json_encode($ret);
    }

    public function get_model_weights($model_id)
    {
        $weights = M("weights")->where('model_id=' . $model_id)->select();
        for ($i = 0; $i < count($weights); $i++) {
            $weights[$i]['verify_cnt'] = M("verify_list")->where('weights_id=' . $weights[$i]['id'])->count();
            $weights[$i]['results_cnt'] = M("results")->where('weights_id=' . $weights[$i]['id'])->count();
        }
        $ret['weights'] = $weights;
        echo json_encode($ret);
    }

    public function add_testing_data($weights_id = 0)
    {
        M("verify_list")->where('weights_id=' . $weights_id)->delete();
        $sql = "INSERT INTO verify_list (weights_id,model_id,class_id,movie_id,frame_id) SELECT weights.id AS weights_id,model.id AS model_id,class.id AS class_id, class.movie_id AS movie_id,frame.frame_index AS frame_id FROM weights JOIN model ON model.id = weights.model_id JOIN model_classes ON model_classes.model_id = model.id JOIN class ON model_classes.class_id = class.id JOIN frame ON frame.movie_id = class.movie_id WHERE weights.id=" . $weights_id;
        $Model = new \Think\Model();
        $ret = $Model->query($sql);
        echo json_encode($ret);
    }

    public function get_weights_movies($weights_id)
    {
        $sql = "SELECT movie.* FROM weights 
JOIN model ON weights.model_id = model.id 
JOIN model_classes ON model.id = model_classes.model_id 
JOIN class ON class.id = model_classes.class_id 
JOIN movie ON class.movie_id = movie.id 
WHERE weights.id =" . $weights_id;
        $Model = new \Think\Model();
        $ret = $Model->query($sql);
        echo json_encode($ret);
    }

    public function get_results_count($weights_id = 0, $movie_id = 0, $confidence = 0.2)
    {
        $sql = 'SELECT count(*) FROM results
LEFT JOIN movie ON movie.id = results.movie_id
WHERE weights_id = $weights_id  AND movie_id =  $movie_id  AND confidence >  $confidence 
ORDER BY frame_id';


        $sql = str_replace('$weights_id', $weights_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$confidence', $confidence, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        $count = $ret[0]['count(*)'];
        return $count;
    }

    public function get_results_frames($weights_id = 0, $movie_id = 0, $confidence = 0.2, $page = 0)
    {
        $page_cnt = 5*100;
        $results_model = new \Think\Model();
        $sql = 'SELECT frame_id, class_id, class_index, movie_id, confidence, w, h, x1, y1, x2, y2 FROM results
LEFT JOIN movie ON movie.id = results.movie_id
WHERE weights_id = $weights_id  AND movie_id =  $movie_id  AND confidence >  $confidence 
ORDER BY frame_id 
LIMIT $page_cnt
OFFSET $offset';


        $sql = str_replace('$weights_id', $weights_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$confidence', $confidence, $sql);
        $sql = str_replace('$page_cnt', $page_cnt, $sql);
        $sql = str_replace('$offset', $page_cnt * $page, $sql);

        $ret = $results_model->query($sql);

        $count = $this->get_results_count($weights_id, $movie_id, $confidence);

        $ret['pages'] = $this->get_pages($page_cnt, $page, $count);

        echo json_encode($ret);
    }

    public function get_pages($page_cnt, $page, $count)
    {
        $pages = array();
        for ($i = 0; $i < ($count / $page_cnt); $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i < (($count / $page_cnt)-1)){
                $data['title'] = $i*$page_cnt . '~' . ($i+1)*$page_cnt ;
            }else{
                $data['title'] = $i*$page_cnt . '~' . $count ;
            }
            $data['cnt'] = $page_cnt;
            if ($i == $page) {
                $data['class'] = 'btn btn-default btn-xs active';
            } else {
                $data['class'] = 'btn btn-default btn-xs ';
            }
            array_push($pages, $data);
        }
        return $pages;
    }

    public function get_action($weights_id = 0)
    {
        $weights = M("weights")->where('id=' . $weights_id)->find();


        $sql1 = 'insert into testing_annotation (annotation_id, model_id)
select id as annotation_id,$model_id from `annotation` where class_id =$class_id
and id not in (select annotation_id from training_annotation where model_id = $model_id)
limit 100';

        $training_sql = '';

        $Model = new \Think\Model();
        $sql = 'SELECT class.*, movie.id as movie_id FROM model_classes JOIN class ON class.id = model_classes.class_id JOIN movie ON movie.id = class.movie_id WHERE model_classes.model_id = $model_id';
        $sql = str_replace('$model_id', $weights['model_id'], $sql);
        $classes = $Model->query($sql);

        foreach ($classes as $class){
            $temp = str_replace('$model_id', $weights['model_id'], $sql1);
            $temp = str_replace('$class_id', $class['id'], $temp);
            $training_sql = $training_sql . $temp . ';';
        }

        $add_iou_sql = "INSERT INTO verify_list (weights_id,model_id,class_id,movie_id,frame_id) SELECT $weights_id, testing_annotation.model_id, annotation.class_id, annotation.movie_id, annotation.frame_index AS frame_id 
 FROM testing_annotation 
 JOIN annotation ON annotation.id = testing_annotation.annotation_id";

        $add_iou_sql = str_replace('$weights_id', $weights['id'], $add_iou_sql);

        $ret['add_iou_sql'] = $add_iou_sql;
        $ret['verify_all_frame'] = "/ai_dl/python/" . $weights['model_id'] . "/darknet.v2.cuda.9.0 verify 0";
        $ret['verify_iou_frame'] = "/ai_dl/python/" . $weights['model_id'] . "/darknet.v2.cuda.9.0 db 0";

        echo json_encode($ret);

    }

    public function reload_weights_list(){
        $ret = shell_exec("python /ai_dl/python/weights_list.py");
        echo $ret;
    }

}
