<?php

namespace Home\Controller;

use Think\Controller;

class MovieController extends Controller
{
    public function index()
    {
        $this->display();
    }

    private function _save($input)
    {
        $where['imdb_id'] = $input['imdb_id'];
        $movie = M("movie")->where($where)->find();
        echo count($movie);
        if (count($movie) == 0) {
            M("movie")->field('imdb_id,title,year,actors,plot,rating,poster,video_file')->add($input);
        } else {
            M("movie")->where($where)->field('imdb_id,title,year,actors,plot,rating,poster,video_file')->save($input);
        }
    }

    public function save()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $data['imdb_id'] = $input['imdbID'];
            $data['title'] = $input['Title'];
            $data['year'] = $input['Year'];
            $data['actors'] = $input['Actors'];
            $data['plot'] = $input['Plot'];
            $data['rating'] = $input['imdbRating'];
            $data['poster'] = $input['Poster'];
            $data['video_file'] = $input['video_file'];
            $this->_save($data);
        }
        return true;
    }

    public function get_movies()
    {
        $movies = M("movie")->select();
        $ret['movies'] = $movies;
        echo json_encode($ret);
    }

    public function get_keyframes_count($movie_id)
    {
        $where['movie_id'] = $movie_id;
        $where['keyframe'] = 1;
        $count = M("frame")->where($where)->count();
        return $count;
    }

    private function get_annotation($frame)
    {
        $where['movie_id'] = $frame['movie_id'];
        $where['frame_index'] = $frame['frame_index'];
        $annotation = M('annotation')->where($where)->find();
        if ($annotation != null) {
            return $annotation;
        } else {
            $w['movie_id'] = $frame['movie_id'];
            $movie = M('movie')->where($w)->find();
            $class = M('class')->where($w)->find();
            $ret['class_id'] = $class['id'];
            $ret['movie_id'] = $frame['movie_id'];
            $ret['frame_id'] = $frame['id'];
            $ret['frame_index'] = $frame['frame_index'];
            $ret['id'] = -1;
            $ret['w'] = $movie['w'];
            $ret['h'] = $movie['h'];
            $ret['x1'] = 0;
            $ret['y1'] = 0;
            $ret['x2'] = 0;
            $ret['y2'] = 0;
            return $ret;
        }
    }

    public function get_key_frames($movie_id, $page)
    {
        $page_cnt = 200;
        $where['movie_id'] = $movie_id;
        $where['keyframe'] = 1;
        $frames = M('frame')->where($where)->limit($page * $page_cnt, $page_cnt)->select();
        for ($i = 0; $i < count($frames); $i++) {
            $frames[$i]['annotation'] = $this->get_annotation($frames[$i]);
        }
        $ret['frames'] = $frames;
        $count = $this->get_keyframes_count($movie_id);
        $ret['pages'] = $this->get_pages($page_cnt, $page, $count);
        $ret['count'] = $count;
        echo json_encode($ret);
    }

    public function get_pages($page_cnt, $page, $count)
    {
        $pages = array();
        for ($i = 0; $i < ($count / $page_cnt); $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i < (($count / $page_cnt) - 1)) {
                $data['title'] = $i * $page_cnt . '~' . ($i + 1) * $page_cnt;
            } else {
                $data['title'] = $i * $page_cnt . '~' . $count;
            }
            $data['cnt'] = $page_cnt;
            if ($i == $page) {
                $data['class'] = 'btn btn-default btn-xs active';
            } else {
                $data['class'] = 'btn btn-default btn-xs ';
            }
            array_push($pages, $data);
        }
        return $pages;
    }
}
