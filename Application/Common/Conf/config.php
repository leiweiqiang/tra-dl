<?php
return array(
  'URL_CASE_INSENSITIVE'  =>  true,  //忽视大小写
  'SESSION_AUTO_START'    => true,   //是否开启session
  'APP_DEBUG' => true,
  'URL_MODEL' => 2,
  'APP_GROUP_LIST' => 'Home', //项目分组设定
  'DEFAULT_GROUP'  => 'Home', //默认分组
  'MODULE_ALLOW_LIST' => array('Home'),
);
